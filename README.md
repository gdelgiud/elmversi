# Elmversi

Reversi game implemented in Elm. Can be compiled and run in a web browser.

The current build is provided in the 'html' directory.

## Dependencies

Only the standard Elm compiler is necessary. Follow the [Install Guide](http://elm-lang.org/Install.elm) for more information.

## Build

In Elmversi's root folder, run the following command

	elm-make src/Elmversi.elm --output html/Elmversi.html

The resulting HTML file will be placed in the 'html' directory.