module Board where

{-|
This Module defines the game's Board type, and several functions to
manipulate it, as well as some functions to operate with the Color of
the game's Tiles.

# Definitions
@docs Board, TileColor

# Constructors
@docs empty, default

# Querying
@docs tileAt, tilesAtPositions, convertableTiles, isFinished, isValidMove,
validMoves, emptyCells, occupiedCells, allLocations, countTiles

# Modifying
@docs putTileAt, putTilesAt

# Helper functions and constants
@docs width, height, opposingColor, isValidLocation

-}

import Dict as Dict
import List (..)

import Location (..)

{-| A Board is a Dictionary which uses Locations as keys, and the TileColor
at each Location as values.
-}
type alias Board = Dict.Dict Location TileColor

{-| Defines the possible colors of each Player
-}
type TileColor = White | Black

opposingColor : TileColor -> TileColor
opposingColor color =
  case color of
    Black -> White
    White -> Black

{-| Returns a Board with no Tiles placed.
-}
empty : Board
empty = Dict.empty

width : Int
width = 8

height : Int
height = 8

{-| Returns true if the given Location is within the board.-}
isValidLocation : Location -> Bool
isValidLocation (x, y) = x >= 0 && y >= 0 && x < width && y < height

{-| Returns the list of tiles between the given position and the edge of the
board in a given direction, including the tile at the given position.-}
tilesInDirection : Location -> Direction -> List Location
tilesInDirection loc direction =
  if | isValidLocation loc -> loc :: tilesInDirection (direction loc) direction
     | otherwise -> []

{-| Returns the default Board, according to Reversi's rules.
-}
default : Board
default = empty |> putTilesAt 
  [((width // 2 - 1, height // 2 - 1), White),
  ((width // 2, height // 2 - 1), Black),
  ((width // 2 - 1, height // 2), Black),
  ((width // 2, height // 2), White)]

{-| Returns the TileColor at the given Location in the given Board.-}
tileAt : Location -> Board -> Maybe TileColor
tileAt loc board = 
  if | isValidLocation loc -> board |> Dict.get loc
     | otherwise -> Nothing

{-| Takes a list of positions and a board, and returns the TileColors
at those positions Invalid positions are filtered out.-}
tilesAtPositions : Board -> List Location -> List (Maybe TileColor)
tilesAtPositions board ts = map (\p -> tileAt p board) ts

{-| Places the given tile in the given position on the given board.
Returns the new board, or the same board if the position is invalid.-}
putTileAt : Location -> TileColor -> Board -> Board
putTileAt position color board =
  if | isValidLocation position -> board |> Dict.insert position color
     | otherwise -> board

{-| Places the list of TileColors provided in the given board
Ignores positions that are out of bounds.-}
putTilesAt : List (Location, TileColor) -> Board -> Board
putTilesAt tiles board =
    foldl (\(pos, color) accBoard -> putTileAt pos color accBoard) board tiles

{-| Takes a list of Tiles and returns, from the first element, how many tiles
can be converted if playing the given Tile color, according to Reversi's rules.-}
convertedTilesAmount : List (Maybe TileColor) -> TileColor -> Int
convertedTilesAmount ts tile = convertedTilesAmountRec ts tile 0

convertedTilesAmountRec : List (Maybe TileColor) -> TileColor -> Int -> Int
convertedTilesAmountRec tiles color acc =
  case tiles of
    [] -> 0
    t :: ts -> if | t == Just (opposingColor color) -> convertedTilesAmountRec ts color (acc + 1)
                  | t == Just color -> acc
                  | otherwise -> 0

{-| Takes a board, a position, a direction and a TileColor, and returns which
Locations in the given direction will be converted
if playing the given TileColor.-}
convertableTilesInDirection : Location -> Direction -> TileColor -> Board -> List Location
convertableTilesInDirection position direction color board =
  let tilesToCheck = tail <| tilesInDirection position direction in
  take (convertedTilesAmount (tilesAtPositions board tilesToCheck) color) tilesToCheck

{-| Takes a board, a position and a TileColor, and returns which tiles will be
converted, in any direction.-}
convertableTiles : Location -> TileColor -> Board -> List Location
convertableTiles position color board =
  let tiles direction = convertableTilesInDirection position direction color board in
  append (tiles up)
  <| append (tiles down)
  <| append (tiles left)
  <| append (tiles right)
  <| append (tiles upLeft)
  <| append (tiles upRight)
  <| append (tiles downLeft)
  (tiles downRight)

{-| Returns true if there are no more moves available in the given Board for
any Player -}
isFinished : Board -> Bool
isFinished board = isEmpty (emptyCells board) || (isEmpty (validMoves White board) && isEmpty (validMoves Black board))

{-| Returns whether the given TileColor can be placed at the given Board in the
given Location or not.-}
isValidMove : Location -> TileColor -> Board -> Bool
isValidMove loc color board =
  let cellContent = board |> tileAt loc in
  case cellContent of
    Nothing -> not (isEmpty (convertableTiles loc color board))
    otherwise -> False

{-| Returns all valid moves for the given color in the given board.-}
validMoves : TileColor -> Board -> List Location
validMoves color board = filter (\p -> isValidMove p color board) <| emptyCells board

{-| Returns the positions of all empty cells in the given board.-}
emptyCells : Board -> List Location
emptyCells board = filter (\x -> not (member x (Dict.keys board))) <| allLocations

{-| Returns the positions of all the cells containing any tile.-}
occupiedCells : Board -> List Location
occupiedCells board = Dict.keys board

{-| Returns a List containing all the Locations inside the board,
as ordered pairs.-}
allLocations : List Location
allLocations = map (\i -> (i `rem` width, i // height)) [0..(width * height) - 1]

{-| Returns the amount of tiles of the given color contained in the given board.-}
countTiles : TileColor -> Board -> Int
countTiles color board = length <| filter (\c -> c == color) (Dict.values board)