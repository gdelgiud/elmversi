module Model where

import Signal
import Maybe (..)
import Graphics.Input.Field as Field

import Board (..)
import Location (Location)

{-
The State represents all possible states the application can be in.
The application can either show the Main Menu, or the Game scene.
-}
type State = Game GameState | Menu MenuState

{-|
A GameState represents the state for the game in any given moment.
It contains the current Board, both Players' records and which player has
to play next (represented with a TileColor).
-}
type alias GameState = {board:Board, turn:TileColor, whitePlayer:Player, blackPlayer:Player}

{-|
A MenuState represents the state for the Main Menu. It contains the
Players' name input.
-}
type alias MenuState = {whitePlayerName:Field.Content, blackPlayerName:Field.Content}

{-|
Contains all data belonging to a Player, such as their name, the score and
their Tile color.
-}
type alias Player = {name:String, color:TileColor, score:Int}

{-|
Lists all the possible kinds of input the application will react to.
-}
type Command =
    NoOp
    | PlayTile Location TileColor
    | PassTurn
    | RestartGame
    | WhitePlayerName Field.Content
    | BlackPlayerName Field.Content
    | StartGame String String

{-|
Returns the Player record that has to play next in the current GameState.
-}
currentPlayer : GameState -> Player
currentPlayer state =
    case state.turn of
        White -> state.whitePlayer
        Black -> state.blackPlayer

{-|
Returns the Player with the highest score in the given GameState,
or returns Nothing if both Players are tied.
-}
winner : GameState -> Maybe Player
winner state =
    if | state.whitePlayer.score > state.blackPlayer.score -> Just state.whitePlayer
       | state.blackPlayer.score > state.whitePlayer.score -> Just state.blackPlayer
       | otherwise -> Nothing

commands : Signal.Channel Command
commands = Signal.channel NoOp
