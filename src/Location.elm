module Location where

{-|
This Module defines the alias Location and helper functions to operate with it.

# Definition
@docs Location

# Direction
@docs Direction, up, down, left, right, upLeft, upRight, downLeft, downRight
-}

{-| A Location is a (x, y) position within the game board -}
type alias Location = (Int, Int)

{-| A Direction is a function that converts one Location into another -}
type alias Direction = (Int, Int) -> (Int, Int)

up : Direction
up (x, y) = (x, y - 1)

down : Direction
down (x, y) = (x, y + 1)

left : Direction
left (x, y) = (x - 1, y)

right : Direction
right (x, y) = (x + 1, y)

upLeft : Direction
upLeft (x, y) = (x - 1, y - 1)

upRight : Direction
upRight (x, y) = (x + 1, y - 1)

downLeft : Direction
downLeft (x, y) = (x - 1, y + 1)

downRight : Direction
downRight (x, y) = (x + 1, y + 1)