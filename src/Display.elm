module Display where

{-| This Module defines several widgets used in the GUI of the game

# General views
@docs screen, primaryText, primaryButton

# Main Menu views
@docs menuScreen, playerNameInput, titleView

# Board views
@docs boardView, cellView, playerTileView, validMoveView, validMoveButton

# Status panel views
@docs statusPanel, playerStatusView, tileCountView, reportText, endText, passTurnButton, restartGameButton

# Utils
@docs toScreenPosition, cellCollage
-}

import Color (Color)
import Graphics.Element (..)
import Graphics.Collage (..)
import Graphics.Input as Input
import Graphics.Input.Field (..)
import Signal
import List (..)
import Maybe (Maybe (Just, Nothing), withDefault)
import Text as Text

import Location (Location)
import Style (..)
import Model (..)
import Board as Board
import Board (Board, TileColor(..))

{-
-- General views
-}

screen : (Int, Int) -> State -> Element
screen (w, h) state =
  case state of
    Menu s -> color secondaryBackgroundColor <| container w h middle <| menuScreen s
    Game s -> color secondaryBackgroundColor <| container w h middle <| gameScreen s

primaryText : String -> Color -> Element
primaryText text color =
  Text.fromString text
    |> Text.color color
    |> Text.typeface primaryTypeface
    |> Text.height (cellSize * 0.5)
    |> Text.centered

primaryButton : Float -> Float -> String -> Command -> Element
primaryButton w h label command =
  let background bgColor outlineColor =
    layers [
      collage (floor w) (floor h) [
          rect w h |> filled bgColor,
          rect w h |> outlined (solid outlineColor)
      ],
      container (floor w) (floor h) middle <| primaryText label blackTextColor
    ]
  in
  Input.customButton (Signal.send commands command)
  (background primaryButtonIdleColor primaryButtonOutlineColor)
  (background primaryButtonHoverColor primaryButtonOutlineColor)
  (background primaryButtonPressedColor primaryButtonOutlineColor)

{-
-- Main Menu views
-}

menuScreen : MenuState -> Element
menuScreen state = color primaryBackgroundColor <|
  container screenWidth screenHeight middle <|
  flow down <| map (container screenWidth iCellSize middle) (menuElements state)

menuElements : MenuState -> List Element
menuElements {whitePlayerName, blackPlayerName} = [
  flow right [lambdaSymbol White, titleView, lambdaSymbol Black],
  empty,
  playerNameInput White "White player's name" whitePlayerName WhitePlayerName,
  playerNameInput Black "Black player's name" blackPlayerName BlackPlayerName,
  empty,
  primaryButton startButtonWidth cellSize "Start game" (StartGame whitePlayerName.string blackPlayerName.string)]

playerNameInput : TileColor -> String -> Content -> (Content -> Command) -> Element
playerNameInput color placeholder content updateFunc =
  cellCollage [(playerTileView color)] `beside`
  (container (floor (startButtonWidth - cellSize)) iCellSize middle <|
  field defaultStyle (Signal.send commands << updateFunc) placeholder content)

titleView : Element
titleView = container (floor startButtonWidth) iCellSize middle (
  Text.fromString "Elmversi"
    |> Text.color blackTextColor
    |> Text.typeface primaryTypeface
    |> Text.height cellSize
    |> Text.centered)

lambdaSymbol : TileColor -> Element
lambdaSymbol color =
    let view path = container iCellSize iCellSize middle <|
        image iCellSize iCellSize path
    in
    case color of
        White -> view "../img/lambda_white.svg"
        Black -> view "../img/lambda_black.svg"

{-
-- Board views
-}

gameScreen : GameState -> Element
gameScreen state = flow right [boardView state.board (Board.validMoves state.turn state.board), statusPanel state]

boardView : Board -> List Location -> Element
boardView board validMoves =
  let cells = map (\p -> cellView cellIdleColor cellOutlineColor |> move (toScreenPosition p)) Board.allLocations in
  let playerTiles = map (\p -> playerTileView (withDefault White (Board.tileAt p board)) |> move (toScreenPosition p)) <| Board.occupiedCells board in
  let validTiles = map (\p -> validMoveButton p White |> move (toScreenPosition p)) <| validMoves in
  collage (iCellSize * Board.width) (iCellSize * Board.width) <| cells ++ playerTiles ++ validTiles

cellView : Color -> Color -> Form
cellView bgColor outlineColor =
  group [
    square cellSize |> filled bgColor,
    square cellSize |> outlined (solid outlineColor)]

playerTileView : TileColor -> Form
playerTileView tileColor =
  let background color =
  group [
    circle tileRadius |> filled color,
    circle tileRadius |> outlined (solid tileOutlineColor)] in
  case tileColor of
    White -> background whiteTileColor
    Black -> background blackTileColor

validMoveView : Form
validMoveView =
  group [
    filled validMoveTileColor (circle (tileRadius / 2)),
    outlined (solid tileOutlineColor) (circle (tileRadius / 2))
  ]

validMoveButton : Location -> TileColor -> Form
validMoveButton loc tileColor =
  toForm <| Input.customButton (Signal.send commands (PlayTile loc tileColor))
    (cellCollage [cellView cellIdleColor cellOutlineColor, validMoveView])
    (cellCollage [cellView cellHoverColor cellOutlineColor, validMoveView])
    (cellCollage [cellView cellPressedColor cellOutlineColor, validMoveView])

{-
-- Status panel views
-}

statusPanel : GameState -> Element
statusPanel state = layers [
    color primaryBackgroundColor <|
    container statusPanelWidth statusPanelHeight topLeft <| flow down [
      (playerStatusView state.whitePlayer state.turn),
      (playerStatusView state.blackPlayer state.turn),
      spacer iCellSize iCellSize,
      reportText state],
    container statusPanelWidth statusPanelHeight midBottom <| passTurnButton `beside` restartGameButton
  ]

playerStatusView : Player -> TileColor -> Element
playerStatusView player turn = container statusPanelWidth iCellSize topLeft <|
  let view =
    flow right [
      tileCountView player.score player.color,
      container statusPanelWidth iCellSize midLeft <| primaryText player.name blackTextColor]
  in
  if | turn == player.color -> color currentPlayerColor <| view
     | otherwise -> view

tileCountView : Int -> TileColor -> Element
tileCountView count tileColor =
  let size = iCellSize in
  let view tileColor textColor = layers [
    collage size size <| [playerTileView tileColor],
    container size size middle <| primaryText (toString count) textColor]
  in
  case tileColor of
    White -> view White blackTextColor
    Black -> view Black whiteTextColor

reportText : GameState -> Element
reportText state =
  if | Board.isFinished state.board -> endText state
     | otherwise -> empty

endText : GameState -> Element
endText state =
  let text result = flow down [
    container statusPanelWidth iCellSize middle <| primaryText "Game Over" blackTextColor,
    container statusPanelWidth iCellSize middle <| primaryText result blackTextColor
  ]
  in
  case winner state of
    Just player -> text (player.name ++ " has won!")
    Nothing -> text "The match ended in a draw"

passTurnButton : Element
passTurnButton = primaryButton (toFloat statusPanelWidth / 2) cellSize "Pass" PassTurn

restartGameButton : Element
restartGameButton = primaryButton (toFloat statusPanelWidth / 2) cellSize "Restart" RestartGame

{-
-- Utils
-}

-- Transforms a board Location to screen position
toScreenPosition : Location -> (Float, Float)
toScreenPosition (x, y) =
  let offsetX = (cellSize / 2) * (toFloat Board.width - 1) in
  let offsetY = (cellSize / 2) * (toFloat Board.width - 1) in
  (-offsetX + toFloat x * cellSize, offsetY - toFloat y * cellSize)

cellCollage : List Form -> Element
cellCollage forms = collage iCellSize iCellSize forms
