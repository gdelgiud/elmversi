module Elmversi where

import Graphics.Element (..)
import Signal
import Window

import Display (..)
import Model (..)
import Update (..)

port title : String
port title = "Elmversi"

port favicon : String
port favicon = "../img/logo.svg"

main : Signal Element
main =
  Signal.subscribe commands
    |> Signal.foldp update initialState
    |> Signal.map2 screen Window.dimensions