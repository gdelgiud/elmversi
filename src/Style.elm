module Style where

import Color (..)

import Board as Board

{-| This Module defines several constantes related to the GUI of the game,
such as colors and dimensions-}

------------------------

primaryTypeface : List String
primaryTypeface = ["Helvetica Neue","Sans-serif"]

------------------------

cellSize : Float
cellSize = 64

iCellSize : Int
iCellSize = floor cellSize

tileSize : Float
tileSize = 0.8 * cellSize

tileRadius : Float
tileRadius = tileSize / 2

statusPanelWidth : Int
statusPanelWidth = (Board.width * (floor cellSize)) // 2

statusPanelHeight : Int
statusPanelHeight = Board.height * (floor cellSize)

screenWidth : Int
screenWidth = Board.width * (floor cellSize) + statusPanelWidth

screenHeight : Int
screenHeight = statusPanelHeight

startButtonWidth : Float
startButtonWidth = 300

maxPlayerNameLength : Int
maxPlayerNameLength = 10

-----------------------

primaryBackgroundColor : Color
primaryBackgroundColor = lightGrey

secondaryBackgroundColor : Color
secondaryBackgroundColor = charcoal

whiteTileColor : Color
whiteTileColor = white

blackTileColor : Color
blackTileColor = greyscale 0.7

tileOutlineColor = black

validMoveTileColor : Color
validMoveTileColor = lightRed

cellIdleColor : Color
cellIdleColor = grayscale 0.2

cellHoverColor : Color
cellHoverColor = grayscale 0.3

cellPressedColor : Color
cellPressedColor = grayscale 0.4

cellOutlineColor : Color
cellOutlineColor = grayscale 0.8

whiteTextColor : Color
whiteTextColor = white

blackTextColor : Color
blackTextColor = black

primaryButtonIdleColor : Color
primaryButtonIdleColor = lightBlue

primaryButtonHoverColor : Color
primaryButtonHoverColor = blue

primaryButtonPressedColor : Color
primaryButtonPressedColor = darkBlue

primaryButtonOutlineColor : Color
primaryButtonOutlineColor = black

currentPlayerColor : Color
currentPlayerColor = lightBlue