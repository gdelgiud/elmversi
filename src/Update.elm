module Update where

{-|
This Module defines several functions that transform one record into another,
allowing the application to step from one state to the next.
-}

import List (..)
import Graphics.Input.Field as Field
import Maybe (Maybe (Just, Nothing), withDefault)

import Location (..)
import Model (..)
import Board (..)

{-|
Given a Command and the current State, decides which scene the application
is in, and returns the resulting State after applying said Command.
-}
update : Command -> State -> State
update command state =
    case state of
        Menu s -> updateMenu command s
        Game s -> updateGame command s

{-| Takes a GameState and a Command, and returns the resulting State after
applying said Command.
-}
updateGame : Command -> GameState -> State
updateGame command state =
  case command of
    NoOp -> Game state
    PlayTile loc color -> Game (withDefault state (playTile loc state))
    PassTurn -> Game (passTurn state)
    RestartGame -> Game (initialGame state.whitePlayer.name state.blackPlayer.name)

{-| Takes a MenuState and a Command, and returns the resulting State after
applying said Command.
-}
updateMenu : Command -> MenuState -> State
updateMenu command state =
    case command of
        WhitePlayerName content -> Menu {state | whitePlayerName <- content}
        BlackPlayerName content -> Menu {state | blackPlayerName <- content}
        StartGame white black -> Game (initialGame white black)
        otherwise -> Menu state

{-|
Given a GameState and a Location, places a Tile with the current Player's
color at said Location in the GameState's board, converts all tiles
according to Reversi's rules, and updates each Player's info. Returns the
resulting GameState if the move is valid, or Nothing otherwise.
-}
playTile : Location -> GameState -> Maybe GameState
playTile loc {board, turn, whitePlayer, blackPlayer} =
    let flippedTiles = convertableTiles loc turn board in
    let nextBoard =
        board
        |> putTilesAt (map (\p -> (p, turn)) flippedTiles)
        |> putTileAt loc turn
    in
    if | isValidMove loc turn board -> Just
    {
        board = nextBoard,
        turn = opposingColor turn,
        whitePlayer = {whitePlayer | score <- countTiles whitePlayer.color nextBoard},
        blackPlayer = {blackPlayer | score <- countTiles blackPlayer.color nextBoard}
    }
       | otherwise -> Nothing

{-|
Given a GameState, returns a new GameState with a switched Player turn.
-}
passTurn : GameState -> GameState
passTurn state = {state | turn <- opposingColor state.turn}

{-|
Returns a GameState containing the default board.
-}
initialGame : String -> String -> GameState
initialGame whitePlayerName blackPlayerName =
    let newBoard = default in
    {
        board = newBoard,
        turn = White,
        whitePlayer = {name = whitePlayerName, color = White, score = countTiles White newBoard},
        blackPlayer = {name = blackPlayerName, color = Black, score = countTiles Black newBoard}
    }

{-|
Returns a Main Menu with both Player's names empty
-}
initialMenu : MenuState
initialMenu = {whitePlayerName = Field.noContent, blackPlayerName = Field.noContent}

{-|
Returns the Main Menu state.
-}
initialState : State
initialState = Menu initialMenu